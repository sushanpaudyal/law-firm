-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2021 at 12:29 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biztechbooster`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `happy_clients` int(11) NOT NULL,
  `teams` int(11) NOT NULL,
  `years` int(11) NOT NULL,
  `projects` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seo_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_subtitle` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `page_title`, `subtitle`, `description`, `happy_clients`, `teams`, `years`, `projects`, `created_at`, `updated_at`, `seo_title`, `seo_subtitle`, `seo_description`, `seo_keywords`) VALUES
(1, 'About Us', 'We are more than just a IT agency.', '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><span style=\"font-family: circularstd;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie vel urna maximus pellentesque. Aliquam sollicitudin sollicitudin semper. In tempor mauris ut quam volutpat, quis accumsan nisi consequat. Vivamus et lacus blandit justo accumsan pulvinar a in ex. Pellentesque ut ornare nunc, eget ornare sem. Aenean aliquam ultrices mi. Aliquam odio felis, euismod nec tempor eget, pulvinar eu nulla. Suspendisse fermentum ligula sit amet neque auctor, ac dapibus libero egestas. Nam eleifend posuere rhoncus. Maecenas sit amet gravida ipsum, ac porta tortor. Proin </span><span style=\"font-family: Verdana;\">venenatis l</span><span style=\"font-family: circularstd;\">ectus et sapien varius, at placerat lorem commodo.</span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><span style=\"font-family: circularstd;\">Nam aliquam, ligula vel posuere facilisis, erat eros consequat nisl, non vulputate lacus nisl in dolor. Vivamus fringilla libero at lectus commodo, at tempus ipsum rutrum. Cras rhoncus metus nibh, id euismod arcu scelerisque sed. Aenean posuere tellus ut mi suscipit, vitae gravida mauris mollis. Maecenas at aliquet enim. Vestibulum luctus ornare porttitor. Etiam vehicula velit nunc, sit amet imperdiet lorem commodo quis. Maecenas id turpis at ligula aliquet sodales efficitur vitae ante. Curabitur libero urna, euismod vel dolor fringilla, dapibus gravida lorem. Morbi fermentum nisi id tempor eleifend. Vivamus congue faucibus urna vitae scelerisque. Donec tincidunt lorem non urna scelerisque laoreet. Mauris efficitur, orci et tincidunt fermentum, lorem neque feugiat massa, et condimentum neque sem vitae erat. Mauris dapibus imperdiet enim a iaculis.</span><span style=\"font-family: circularstd;\">﻿</span></p>', 20, 30, 4, 10, NULL, '2021-12-10 13:39:36', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `image`, `mobile`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Biz Tech Booster', 'sushan.paudyal@gmail.com', '$2y$10$D30y0cLZFtmvA14XWHd1VO2LZjo3IWNKKFA7H7jgfjcB.yGDOm.F6', 'rdhdgPrf3i.jpeg', '9803961735', 'Shantinagar, Kathmandu', '2021-12-10 08:34:58', '2021-12-10 08:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `discover_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `subtitle`, `image`, `info`, `discover_link`, `created_at`, `updated_at`) VALUES
(1, 'Improve your business', 'Inspire the Next', 'YwMOKhKzqs.jpg', 'BizTechBooster is an information technology company that focuses on quality, innovation, & speed. We utilize the latest technology, to bring results, we help and support for growing the businesses. We pride ourselves in great work ethic, integrity, and end-results.', NULL, NULL, '2021-12-10 09:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `website_link`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Mount Everest', NULL, '0yaekx12mO.jpeg', '2021-12-10 14:22:05', '2021-12-10 14:22:05'),
(3, 'Lavya Holidays', NULL, 'nESrJ6OTfn.jpeg', '2021-12-10 14:22:43', '2021-12-10 14:22:43'),
(4, 'Sky Diary', 'http://techcoderznepal.com/', 'NEEoaJzZA7.png', '2021-12-10 14:23:03', '2021-12-10 14:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_10_01_101832_create_admins_table', 1),
(6, '2021_11_01_192814_create_themes_table', 1),
(7, '2021_12_10_142433_create_banners_table', 2),
(8, '2021_12_10_150435_create_socials_table', 3),
(9, '2021_12_10_154035_create_services_table', 4),
(10, '2021_12_10_161250_add_seo_settings_to_services_table', 5),
(11, '2021_12_10_180002_create_service_details_table', 6),
(12, '2021_12_10_190046_create_abouts_table', 7),
(13, '2021_12_10_191540_add_seo_details_to_abouts_table', 8),
(14, '2021_12_10_194607_create_clients_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `status` enum('Active','In Active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seo_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_subtitle` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_name`, `slug`, `icon`, `priority`, `status`, `description`, `image`, `created_at`, `updated_at`, `seo_title`, `seo_subtitle`, `seo_description`, `seo_keywords`) VALUES
(1, 'Mobile App Development', 'mobile-app-development', '55525146288092.png', 1, 'Active', '<p style=\"\"><font color=\"#424242\" face=\"circularstd\"><span style=\"font-size: 16px;\">Mobile app development is the process of creating a software that runs on mobile devices which are developed in a way to take advantage of those devices\' features and it’s hardware. Mobile apps today have become a basic need for everyone as the mobile traffic is outpacing desktop traffic in most of the cases. The use of the mobile phone has increased the demand for mobile apps in the market. Mobile apps can be used anytime anywhere from the mobile phone home screen so they\'re very accessible. Here at Tech Coderz, we build mobile apps according to your requirements so that information about your business can be accessed easily right from the fingertips which will increase&nbsp;your business. Mobile apps are more suitable to re-engage with the users than just a responsive website. With the mobile app, you can keep the users updated about anything regarding your business; from new products to sales to other services you provide by sending notifications to them.</span></font></p><p style=\"margin-bottom: 1rem; font-family: Poppins, sans-serif; color: rgb(66, 66, 66); font-size: 16px;\"><span style=\"font-family: circularstd;\">The creation of mobile application also roots back to traditional software development. But in the mobile application, it is formulated in such a way that it utilizes features and hardware of the mobile device. Our skilled developers\' team provide such mobile app development with end-to-end solutions, based on cutting-edge technology along with modern design and style. In addition, our objective is to work on maintaining simple and user-friendly interfaces, easy to use for you and your customers; even with the complex functionalities. At Tech Coderz, we create applications that are compatible with all kind of devices, that meets the requirement of our clients.&nbsp;</span></p>', '2331277578.jpg', '2021-12-10 10:50:40', '2021-12-10 10:50:40', NULL, NULL, NULL, NULL),
(2, 'Digital / Social Media Marketing', 'digital-social-media-marketing', '80023019027097.png', 2, 'Active', '<p style=\"font-family: Poppins, sans-serif; color: rgb(66, 66, 66); font-size: 16px;\">In times like this, where social media and internet rules everything from business to brands, old methods of marketing are not sufficient. Old marketing techniques don\'t suffice for taking your business to the new heights. &nbsp;As the world is becoming digitalized where people access everything through the smartphones, incorporating new marketing techniques such as Digital/ Social Media Marketing will not only reach people around you but also people all over the internet who falls under your targeted category.&nbsp;</p><p style=\"font-family: Poppins, sans-serif; color: rgb(66, 66, 66); font-size: 16px;\"><br>Digital Marketing and Social Media Marketing are different techniques, but they fall under the same idea of marketing .i.e. through digital platforms. Digital Media Marketing includes email marketing, optimizing websites, online advertisements or banner advertisement whereas Social Media Marketing includes what it entails in its name .i.e. marketing through social media platforms like Facebook, Twitter, Instagram and many more. Social Media Marketing works but advertising posts from social media, forums and blogs to certain class or category of people .</p><p style=\"margin-bottom: 1rem; font-family: Poppins, sans-serif; color: rgb(66, 66, 66); font-size: 16px;\">Here at Tech Coderz, we provide service of both Digital/ Social Media Marketing in reasonable packages with which you not only get benefits of putting your business out there on the internet market but also get content creation and graphic designs for your business. We make sure that we entail everything that is needed for your business to be recognized on the internet since our main motto is&nbsp; “<span style=\"font-weight: bolder;\">clients satisfaction is our satisfaction</span>”.</p>', '7971574904.jpg', '2021-12-10 11:16:20', '2021-12-10 11:16:20', NULL, NULL, NULL, NULL),
(3, 'Graphics Design', 'graphics-design', '67601862809939.png', 3, 'Active', '<p><span style=\"color: rgb(66, 66, 66); font-family: circularstd; font-size: 16px;\">Graphic designing is the key to make your brand professional and way to optimize your marketing efforts. Creating a distinct image of your brand will help with your company being easily recognizable but also allow your customers and clients to be familiar with your business and the services you provide. The contentment of your customers with your consistency marketing and branding will lead to credibility. A strong visual foundation of your brand shows your confidence in its offerings and expertise. This allows customers to engage with your business more frequently thus creating more traffic and sales for you.</span><br></p>', '4979306743.jpg', '2021-12-10 11:18:12', '2021-12-10 11:31:44', NULL, NULL, NULL, NULL),
(4, 'Web Development', 'web-development', '62997587898275.png', 4, 'Active', '<p><span style=\"color: rgb(66, 66, 66); font-family: circularstd; font-size: 16px;\">Websites today are far more than simple information providers. Everyone access website for their day to day activities like looking for the weather, news, buying their everyday necessities. It has got the point that every part of our everyday lives is encompassed in it. Websites can vary from being single page static websites to just give you information to multi-pages dynamic websites with which you can interact with. With the rapid increase of modernization in technology, the online presence of any business is very important. Your online presence is very detrimental in deciding the worth of your company on the internet. Websites can be a powerful business tool, rich in functionalities which not only give your client information about what they need but also interactive and easy to access. Team Tech Coderz has the ability to understand the requirements of your company and to configure features and functions to your satisfaction and thus your client\'s.&nbsp;</span><br></p>', '8968293244.jpg', '2021-12-10 11:19:49', '2021-12-10 11:19:49', NULL, NULL, NULL, NULL),
(7, 'UI/UX Designing', 'uiux-designing', '43322127093730.png', 5, 'Active', '<p><span style=\"font-family: circularstd;\">﻿</span><span style=\"color: rgb(66, 66, 66); font-family: circularstd; font-size: 16px;\">UX/UI also known as User Experience and Interface design. It is the process of designing interfaces for websites, software or application that are user friendly and with focus on usability and experience for the users. Any business wants their sales to increase and grow their business in a large span, but with so many similar businesses in the market, it is hard to catch the attention of customers. It is very important to catch the attention of the customers in the small time since people would not spend hours delving on the same thing. UI/UX designing helps with catching people attention in a short amount of time. It is one of the major tools for marketing your brand. When people\'s attention gets caught they spend time engaging in it and thus creating more traffic, which eventually helps with sales and growing your business.</span><br></p>', '7892732425.jpg', '2021-12-10 11:35:37', '2021-12-10 11:35:37', NULL, NULL, NULL, NULL),
(8, 'Search Engine Optimization (SEO)', 'search-engine-optimization-seo', '11044741517557.png', 6, 'Active', '<p><span style=\"color: rgb(66, 66, 66); font-family: circularstd; font-size: 16px;\">Search Engine Optimization (SEO) is a process to improve your website\'s visibility on the internet. With the better visibility of your websites in search results, it is likely that you are going to gain attention from prospective and existing customers to your business. Google search is one of the main source of digital traffic for any brands. In google search, only 20% of the traffic goes to Google Ads and 80% goes to the search results. Financially speaking, SEO is more money appropriate since more monetary value goes in the ads and very less in Search Engine Optimization. With the help of Search Engine Optimization, it is possible to improve your business visibility in google search. An inside SEO joke is that, If you kill someone, hide their body on the page of google since no one reaches there. So it is very important for business owners to be on the top pages of Google search page since its one of the major factor of digital traffic on your website. With search engine optimization, it improves your site to increase its visibility and grab the attention of users for relevant searches in various search engine platform.</span><br></p>', '6482601111.jpg', '2021-12-10 11:36:16', '2021-12-10 11:36:16', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_details`
--

CREATE TABLE `service_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `get_started` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_details`
--

INSERT INTO `service_details` (`id`, `service_id`, `title`, `description`, `priority`, `get_started`, `created_at`, `updated_at`) VALUES
(1, 1, 'Enterprise web application', '<p><span style=\"color: rgb(66, 66, 66); font-family: circularstd; font-size: 15px;\">Enterprise Web Application is designed in a way to allow you to manage and record the internal and external operations of your company. They are very scalable, accessible via the web, collaborative, secure, data-centered, and easy to use in any companies. You can collaborate with your team, share documents, and increase the productivity of the company. They are both cost and time savings to use and can be accessed from any site on the internet. If you want to create one for your company and manage your operations and increase productivity, feel free to contact us.</span><br></p>', 1, NULL, '2021-12-10 12:46:07', '2021-12-10 12:46:07'),
(2, 1, 'Informative web site', '<p><span style=\"color: rgb(66, 66, 66); font-family: circularstd; font-size: 15px;\">Informative websites are built with the sole purpose of giving information to users. Some of the fields that may use informative websites are such as News, Science, Encyclopedias, Business news websites, Websites giving analysis on some subject, Medical information websites and Educational websites. Even if they\'re just there to provide information, it is also important for them to be visually appealing and easy to use. If you need to create one for your company to give your customers information, feel free to contact us.</span><br></p>', 2, NULL, '2021-12-10 12:47:06', '2021-12-10 12:47:06'),
(4, 1, 'Ecommerce Web App/Site', '<p><span style=\"color: rgb(66, 66, 66); font-family: circularstd; font-size: 15px;\">With everything turning digital these days, it is important for your business to be online too. Ecommerce website or app can be the way to move your sales online. These websites are designed in a way that you can put your products on it where your customers can access them and know all information about products and buy them through that. You can either make payments online or through delivery, it is an easier way for the customers to buy your products. Online e-commerce company tend to have more sales than physical shops these days. So, if you want your own e-commerce web app/site, feel free to contact us.</span><br></p>', 3, NULL, '2021-12-10 12:59:19', '2021-12-10 12:59:19');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `facebook`, `instagram`, `youtube`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://www.youtube.com/', NULL, NULL, '2021-12-10 09:46:29');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `logo`, `favicon`, `website_name`, `created_at`, `updated_at`) VALUES
(1, 'biz-tech-booster-2021-12-10.png', 'favicon-2021-12-10.jpeg', 'Biz Tech Booster', NULL, '2021-12-10 08:38:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_service_name_unique` (`service_name`);

--
-- Indexes for table `service_details`
--
ALTER TABLE `service_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `service_details`
--
ALTER TABLE `service_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
