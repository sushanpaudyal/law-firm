-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2022 at 10:48 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sunny`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us_pages`
--

CREATE TABLE `about_us_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_info` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `our_mission` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `our_vision` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us_pages`
--

INSERT INTO `about_us_pages` (`id`, `page_name`, `title`, `subtitle`, `image`, `page_info`, `our_mission`, `our_vision`, `created_at`, `updated_at`) VALUES
(1, 'About Us', 'About Us', 'Know More About Us', '3274762116.jpg', '<p style=\"margin-bottom: 0px; color: rgb(131, 131, 131); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore etae magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class=\"pb-3\" style=\"color: rgb(131, 131, 131); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<div><span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">To provide customized solutions to the students aspiring to study.</span><br></div><div><ul style=\"list-style: none; padding-left: 0px; color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\"><li style=\"display: flex; line-height: 35px;\"><div><span class=\"fas fa-check mr-2\" style=\"color: var(--yellow-color);\"></span></div>Strive for the organic growth of our organization through integrity, honesty, and excellence.</li><li style=\"display: flex; line-height: 35px;\"><div><span class=\"fas fa-check mr-2\" style=\"color: var(--yellow-color);\"></span></div>To build a strong and credible relationship with the partner institutions by recognizing shared values &amp; goals.</li><li style=\"display: flex; line-height: 35px;\"><div><span class=\"fas fa-check mr-2\" style=\"color: var(--yellow-color);\"></span></div>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt, commodi!</li></ul></div>', '<ul style=\"list-style: none; padding-left: 0px; color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\"><li style=\"display: flex; line-height: 35px;\">To provide customized solutions to the students aspiring to study.</li><li style=\"display: flex; line-height: 35px;\"><div><span class=\"fas fa-check mr-2\" style=\"color: var(--yellow-color);\"></span></div>Strive for the organic growth of our organization through integrity, honesty, and excellence.</li><li style=\"display: flex; line-height: 35px;\"><div><span class=\"fas fa-check mr-2\" style=\"color: var(--yellow-color);\"></span></div>To build a strong and credible relationship with the partner institutions by recognizing shared values &amp; goals.</li><li style=\"display: flex; line-height: 35px;\"><div><span class=\"fas fa-check mr-2\" style=\"color: var(--yellow-color);\"></span></div>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt, commodi!</li></ul>', NULL, '2022-01-15 00:29:07');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `image`, `mobile`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Law Firm Website', 'admin@gmail.com', '$2y$10$qGfrU0l6Qq9ohsmqhpQczOtGLxT.vYin.34dwc6C00i3h5jTVY..6', 'j4YliXBXEr.jpg', '9803961735', 'Shantinagar, Kathmandu', '2022-01-14 23:06:41', '2022-01-15 02:38:23');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `subtitle`, `image`, `link`, `priority`, `created_at`, `updated_at`) VALUES
(1, 'We provide consulting Program to everyone', 'We provide complete consultation', '8063417161.jpg', NULL, 1, '2022-01-14 23:43:24', '2022-01-14 23:43:24'),
(2, 'Have A Problem Contact Us', 'We are there for you', '7306131630.jpg', NULL, 2, '2022-01-14 23:46:38', '2022-01-14 23:46:38');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT 0,
  `blog_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_subtitle` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `blog_title`, `slug`, `view_count`, `blog_content`, `image`, `seo_title`, `seo_subtitle`, `seo_description`, `seo_keywords`, `created_at`, `updated_at`) VALUES
(2, 'Neque porro quisquam est qui dolorem ipsum quia dolor si', 'neque-porro-quisquam-est-qui-dolorem-ipsum-quia-dolor-si', 0, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate magna id massa viverra sollicitudin. Integer scelerisque varius risus. Mauris vel augue tincidunt, euismod nisi sit amet, placerat neque. Etiam condimentum leo at interdum consequat. Praesent dapibus, neque blandit ornare blandit, eros massa aliquet est, ac porttitor mi ex sed mi. Duis rhoncus scelerisque malesuada. Etiam id dolor sit amet odio convallis facilisis. Praesent viverra euismod mauris, vel posuere tellus. Suspendisse posuere sapien at nisl dictum, quis feugiat est congue. Phasellus vestibulum massa quis semper dignissim. Vestibulum in elit fermentum neque mollis pulvinar. Aliquam nec magna magna.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Suspendisse potenti. Fusce euismod ultricies lectus vitae egestas. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum vulputate ornare mi, a varius metus laoreet in. Nunc turpis lacus, efficitur eget ipsum id, dictum aliquam nunc. Vivamus eleifend dignissim lectus, at sagittis dolor consectetur nec. Curabitur nec ex est. Maecenas id ligula facilisis, mollis purus vel, rhoncus lacus. In mattis elementum enim, suscipit iaculis odio ullamcorper et. Mauris eu felis metus. Etiam id sodales nisi, a placerat risus. Sed eros purus, mattis in est vitae, porta tincidunt dolor. Suspendisse vestibulum tincidunt elit, eget fermentum neque varius et. Nam id bibendum ante.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Aliquam eu eleifend mi. Duis leo nunc, elementum sit amet scelerisque id, eleifend et ante. Ut pulvinar enim ac urna pulvinar gravida. Integer eu vulputate velit. Nullam suscipit, nulla nec aliquam dignissim, augue libero pellentesque justo, a aliquam velit dui at magna. Praesent luctus euismod imperdiet. Fusce vitae ultrices est. Vivamus lacinia odio a odio vulputate, pulvinar tincidunt ex scelerisque. Aenean convallis sagittis varius. Duis blandit, metus eget dignissim varius, ligula nulla convallis purus, eget mattis felis augue non ligula.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Sed libero ante, imperdiet sed est nec, pulvinar tempus tellus. Integer at tincidunt erat. Curabitur imperdiet rhoncus mi. Etiam porttitor magna id feugiat ullamcorper. Phasellus vel nulla tempor, maximus quam a, dignissim leo. Vestibulum nibh lacus, pharetra nec convallis eu, ultrices at tortor. Sed tristique enim elit, nec molestie enim finibus egestas. Aliquam quis nulla mollis diam cursus sagittis. Proin eget justo ornare, porta felis ac, suscipit nisl. Vestibulum in posuere dui, eu vulputate ipsum. Nam a cursus eros. Maecenas ut urna aliquam, pharetra risus sed, pharetra ex. Duis eu nisl auctor, pharetra tellus sit amet, viverra turpis.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Proin sit amet dui eros. Ut vulputate risus vitae urna faucibus ornare. In in dignissim lectus, eu aliquam erat. Cras a nulla ut lectus ullamcorper euismod sit amet at velit. Vivamus vitae sagittis metus. Nunc auctor pulvinar metus, ut euismod lorem pulvinar a. Pellentesque sodales dolor felis, non congue purus accumsan eget. Suspendisse nec justo volutpat, posuere neque sed, facilisis mi. Sed placerat arcu sed lorem dignissim, sit amet tempus nunc pharetra. Maecenas sollicitudin cursus odio in eleifend.</p>', '6497828016.png', NULL, NULL, NULL, NULL, '2022-01-15 02:12:49', '2022-01-15 02:12:49'),
(3, '\"There is no one who loves pain itself, who seeks af', 'there-is-no-one-who-loves-pain-itself-who-seeks-af', 0, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">risus. Mauris vel augue tincidunt, euismod nisi sit amet, placerat neque. Etiam condimentum leo at interdum consequat. Praesent dapibus, neque blandit ornare blandit, eros massa aliquet est, ac porttitor mi ex sed mi. Duis rhoncus scelerisque malesuada. Etiam id dolor sit amet odio convallis facilisis. Praesent viverra euismod mauris, vel posuere tellus. Suspendisse posuere sapien at nisl dictum, quis feugiat est congue. Phasellus vestibulum massa quis semper dignissim. Vestibulum in elit fermentum neque mollis pulvinar. Aliquam nec magna magna.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Suspendisse potenti. Fusce euismod ultricies lectus vitae egestas. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum vulputate ornare mi, a varius metus laoreet in. Nunc turpis lacus, efficitur eget ipsum id, dictum aliquam nunc. Vivamus eleifend dignissim lectus, at sagittis dolor consectetur nec. Curabitur nec ex est. Maecenas id ligula facilisis, mollis purus vel, rhoncus lacus. In mattis elementum enim, suscipit iaculis odio ullamcorper et. Mauris eu felis metus. Etiam id sodales nisi, a placerat risus. Sed eros purus, mattis in est vitae, porta tincidunt dolor. Suspendisse vestibulum tincidunt elit, eget fermentum neque varius et. Nam id bibendum ante.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Aliquam eu eleifend mi. Duis leo nunc, elementum sit amet scelerisque id, eleifend et ante. Ut pulvinar enim ac urna pulvinar gravida. Integer eu vulputate velit. Nullam suscipit, nulla nec aliquam dignissim, augue libero pellentesque justo, a aliquam velit dui at magna. Praesent luctus euismod imperdiet. Fusce vitae ultrices est. Vivamus lacinia odio a odio vulputate, pulvinar tincidunt ex scelerisque. Aenean convallis sagittis varius. Duis blandit, metus eget dignissim varius, ligula nulla convallis purus, eget mattis felis augue non ligula.</p>', '8465442643.png', NULL, NULL, NULL, NULL, '2022-01-15 02:13:06', '2022-01-15 02:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

CREATE TABLE `contact_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_messages`
--

INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Sushan paudyal', 'sushan.paudyal@gmail.com', '9803961735', 'This is a new message from my side', '2022-01-15 02:34:43', '2022-01-15 02:34:43'),
(2, 'John Doe', 'myemail@gmail.com', '298198282', 'sadkjdsjamsnd saSDKLJAs aLKSA saLSBA saSBAjs mnsakS AmsvaS AsbvaS DBVSADX ASJMDV ASSDK Asa SHAvskja SKJAsa SKHAvna', '2022-01-15 02:36:26', '2022-01-15 02:36:26'),
(3, 'John Doe', 'myemail@gmail.com', '298198282', 'sadkjdsjamsnd saSDKLJAs aLKSA saLSBA saSBAjs mnsakS AmsvaS AsbvaS DBVSADX ASJMDV ASSDK Asa SHAvskja SKJAsa SKHAvna', '2022-01-15 02:36:58', '2022-01-15 02:36:58'),
(4, 'dssdsad', 'dsadasd@gmail.com', '98709709', 'asndlsndsandsa slksad salkdnsad snldsabdsa lksdnbads', '2022-01-15 02:38:36', '2022-01-15 02:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `title`, `document`, `created_at`, `updated_at`) VALUES
(1, 'Nepal Law', 'public/uploads/document/1642232044.pdf', '2022-01-15 01:49:04', '2022-01-15 01:49:04'),
(3, 'Constitution of Nepal', 'public/uploads/document/1642232489.doc', '2022-01-15 01:56:29', '2022-01-15 01:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_10_01_101832_create_admins_table', 1),
(6, '2021_11_01_192814_create_themes_table', 1),
(7, '2021_12_10_150435_create_socials_table', 1),
(8, '2021_12_15_094853_create_blogs_table', 1),
(9, '2021_12_15_104241_create_settings_table', 1),
(10, '2021_12_16_050746_add_site_title_to_settings_table', 1),
(11, '2022_01_15_051035_create_banners_table', 2),
(12, '2022_01_15_055047_create_about_us_pages_table', 3),
(13, '2022_01_15_061535_create_services_table', 4),
(14, '2022_01_15_064616_create_testimonials_table', 5),
(15, '2022_01_15_072256_create_documents_table', 6),
(16, '2022_01_15_081341_create_contact_messages_table', 7),
(17, '2022_01_15_093541_create_roles_table', 8),
(18, '2022_01_15_094258_add_footer_info_to_settings_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', NULL, NULL),
(2, 'Lawyer', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_info` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `slug`, `subtitle`, `service_info`, `created_at`, `updated_at`) VALUES
(1, 'Criminal Things', 'criminal-things', 'This is the info', '<span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias doloremque, quis, eaque minus harum modi eius veritatis consequuntur expedita impedit ad, facilis. Asperiores assumenda aperiam atque, accusamus cupiditate vero sit!</span>', '2022-01-15 00:45:54', '2022-01-15 00:45:54'),
(3, 'Employment Law', 'employment-law', 'Employment Law', '<span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias doloremque, quis, eaque minus harum modi eius veritatis consequuntur expedita impedit ad, facilis. Asperiores assumenda aperiam atque, accusamus cupiditate vero sit!</span>', '2022-01-15 00:52:04', '2022-01-15 00:52:04'),
(4, 'Estate Planning', 'estate-planning', 'Estate PlanningEstate Planning', '<span style=\"color: rgb(33, 37, 41); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias doloremque, quis, eaque minus harum modi eius veritatis consequuntur expedita impedit ad, facilis. Asperiores assumenda aperiam atque, accusamus cupiditate vero sit!</span>', '2022-01-15 00:53:01', '2022-01-15 00:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `address`, `phone`, `alt_phone`, `email`, `map`, `created_at`, `updated_at`, `site_title`, `site_subtitle`, `footer_info`) VALUES
(1, 'Shantinagar Gate kathmandu', '9801183031', '9803311899', 'sanatansubba@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.982418110285!2d85.34027271490095!3d27.686938282800376!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1980e9319e7f%3A0x220dee5bf8a34f6!2sTech%20Coderz!5e0!3m2!1sen!2snp!4v1639377583043!5m2!1sen!2snp', NULL, '2022-01-15 03:59:56', 'Law Firm', 'The Best U Can Get', 'Our aim mission is to serve its clients and society by providing advanced, accurate, and effective engineering solutions.');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `facebook`, `instagram`, `youtube`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com/', NULL, NULL, NULL, NULL, '2022-01-15 04:02:34');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `position`, `details`, `created_at`, `updated_at`) VALUES
(1, 'Sushan Paudyal', '2377473070.jpg', 'Hello', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vehicula blandit scelerisque. Integer sagittis nisl vitae pharetra tempus. Aenean elementum semper metus eget porttitor.</span>', '2022-01-15 01:19:40', '2022-01-15 01:19:40'),
(3, 'Sunny Limbu', '5986689054.jpg', 'Position', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Curabitur dui neque, posuere nec hendrerit posuere, sagittis nec dolor. Curabitur rhoncus nunc a ipsum consectetur mollis. Suspendisse vitae blandit magna, malesuada rhoncus lorem. Pellentesque quis lacus quis nunc luctus fringilla. Nam nisl justo, semper sed efficitur quis, pellentesque ac dui.</span>', '2022-01-15 01:30:25', '2022-01-15 01:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `logo`, `favicon`, `website_name`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Law Firm', NULL, '2022-01-14 23:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us_pages`
--
ALTER TABLE `about_us_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_messages`
--
ALTER TABLE `contact_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us_pages`
--
ALTER TABLE `about_us_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_messages`
--
ALTER TABLE `contact_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
